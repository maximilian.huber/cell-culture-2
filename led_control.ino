#include <Adafruit_NeoPixel.h> 
#define PIN 2 // input pin Neopixel is attached to
#define NUMPIXELS      40 // number of neopixels in Ring
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// ------------------------ ↓ USER INPUT BELOW ↓ ------------------------ //

int redColor = 0; // EDIT AMOUNT OF LIGHT COLOR
int greenColor = 255;
int blueColor = 0;

boolean runForever = true; // IF INDIVIDUALLY TIMED, SET TRUE

int numberOfLoops = 0; // SET UNUSED PARAMETER TO 0
double numberOfHours = 0.016;

double timeOnPerLoop = .8; // IN SECONDS
double timeOffPerLoop = .2;

// ------------------------ ↑ USER INPUT ABOVE ↑ ------------------------ //

// ------------------------ 10 LIGHTS PER ROW -----------------------------//

void setup() {
    pixels.begin(); // Initializes the NeoPixel library.
}

void set(int r, int g, int b, int num){
    for(int i = 0 ; i < num ; i++){
      if (i == 4 || i == 5 || i == 14 || i == 15 || i == 24 || i == 25|| i == 34 || i == 35){
          pixels.setPixelColor(i, pixels.Color(.95 * r,.95 * g,.95 * b)); // Moderately bright green color.
      }
      else{
          pixels.setPixelColor(i, pixels.Color(r, g, b)); // Moderately bright green color.
      }
      pixels.show(); // This sends the updated pixel color to the hardware.
      delay(0); // Delay for a period of time (in milliseconds).  
  }
}

double calcLoops(double numHours,double numLoops){
  if (numHours < 0.001){
    return numLoops;
  }
  return (numHours * 3600) / (timeOnPerLoop + timeOffPerLoop);
}

boolean isDone = false;
void loop() {
  if (runForever){
      set(redColor,greenColor,blueColor,NUMPIXELS);  
      delay (timeOnPerLoop * 1000);
      if (timeOffPerLoop > 0){
        set(0,0,0,NUMPIXELS);
        delay (timeOffPerLoop * 1000);
      }
  }
  else{
    if(isDone){
      set(0,0,0,NUMPIXELS);
    }
    else{
      for (int j = 0; j < calcLoops(numberOfHours,numberOfLoops); j++){
        set(redColor,greenColor,blueColor,NUMPIXELS);  
        delay (timeOnPerLoop * 1000);
        if (timeOffPerLoop > 0){
          set(0,0,0,NUMPIXELS);
          delay (timeOffPerLoop * 1000);
        }
      }
    }
    isDone = true;
  }
}
  //}

  //isDone = true;

}


